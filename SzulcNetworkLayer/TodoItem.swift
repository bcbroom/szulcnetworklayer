//
//  TodoItem.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/5/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

public struct TodoItem: ParsedItem {
  
  public let id: Int?
  public let title: String
  public let completed: Bool
  public let userID: Int
  
}
