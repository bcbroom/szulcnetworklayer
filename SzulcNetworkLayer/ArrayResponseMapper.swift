//
//  ArrayResponseMapper.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/6/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

final class ArrayResponseMapper<A: ParsedItem> {
  
  static func process(_ obj: Any?, mapper: ((Any?) throws -> A)) throws -> [A] {
    guard let json = obj as? [[String: Any]] else { throw ResponseMapperError.Invalid }
    
    var items = [A]()
    
    for jsonNode in json {
      let item = try mapper(jsonNode)
      items.append(item)
    }
    return items
  }
  
}
