//
//  ResponseMapper.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/6/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

public protocol ResponseMapperProtocol {
  associatedtype Item
  static func process(obj: Any?) throws -> Item
}

internal enum ResponseMapperError: Error {
  case Invalid
  case MissingAttribute
  case CannotParseResponse
}

class ResponseMapper<A: ParsedItem> {
  
  static func process(obj: Any?, parse: (_ json: [String: Any]) -> A?) throws -> A {
    
    guard let json = obj as? [String: Any] else {
      throw ResponseMapperError.Invalid
    }
    
    if let item = parse(json) {
      return item
    } else {
      print("Mapper failure (\(self)). Missing attribute.")
      throw ResponseMapperError.MissingAttribute
    }
    
  }
  
}
