//
//  BackendAuth.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/5/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

public final class BackendAuth {
  
  private let key = "BackendAuthToken"
  private let defaults: UserDefaults
  
  public static var shared: BackendAuth!
  
  public init(defaults: UserDefaults) {
    self.defaults = defaults
  }
  
  public func setToken(token: String) {
    defaults.set(token, forKey: key)
  }
  
  public var token: String? {
    return defaults.value(forKey: key) as? String
  }
  
  public func deleteToken() {
    defaults.removeObject(forKey: key)
  }
  
}
