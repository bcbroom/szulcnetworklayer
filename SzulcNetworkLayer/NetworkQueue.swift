//
//  NetworkQueue.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/5/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

public class NetworkQueue {
  
  public static var shared: NetworkQueue!
  
  let queue = OperationQueue()
  
  public init() { }
  
  public func add(operation: Operation) {
    queue.addOperation(operation)
  }
  
}
