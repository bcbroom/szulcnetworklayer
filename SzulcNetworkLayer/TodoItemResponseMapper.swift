//
//  TodoItemResponseMapper.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/5/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

final class TodoItemResponseMapper: ResponseMapper<TodoItem>, ResponseMapperProtocol {

  static func process(obj: Any?) throws -> TodoItem {
    return try super.process(obj: obj, parse: { json in
      
      let id = json["id"] as? Int
      let userID = json["userId"] as? Int
      let title = json["title"] as? String
      let completed = json["completed"] as? Bool
      
      if let id = id, let userID = userID, let title = title, let completed = completed {
        return TodoItem(id:id, title: title, completed: completed, userID: userID)
      }
      
      return nil

//      guard let id = id, let userID = userID, let title = title, let completed = completed else {
//       return nil
//      }
//
//      return TodoItem(id:id, title: title, completed: completed, userID: userID)
    })
  }
  
}
