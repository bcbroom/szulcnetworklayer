//
//  TodoGetRequest.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/5/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

final class TodoGetRequest: BackendAPIRequest {
  
  private let id: Int
  
  init(id: Int) {
    self.id = id
  }
  
  var endpoint: String {
    return "/todos/\(id)"
  }
  
  var method: NetworkService.Method {
    return .GET
  }
  
  var parameters: [String : Any]? {
    return nil
  }
  
  var headers: [String : String]? {
    return defaultJSONHeaders()
  }
  
}
