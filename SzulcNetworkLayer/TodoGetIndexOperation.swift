//
//  TodoGetIndexOperation.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/19/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

public class TodoGetIndexOperation: ServiceOperation {
  private let request: TodoGetIndexRequest
  
  public var success: (([TodoItem]) -> Void)?
  public var failure: ((NSError) -> Void)?
  
  public override init() {
    request = TodoGetIndexRequest()
    super.init()
  }
  
  public override func start() {
    super.start()
    service.request(request: request, success: handleSuccess, failure: handleFailure)
  }
  
  private func handleSuccess(response: Any?) {
    do {
//      let todoItems = try TodoItemResponseMapper.process(obj: response)
      let todoItems = try ArrayResponseMapper.process(response, mapper: TodoItemResponseMapper.process)
      success?(todoItems)
      finish()
    } catch {
      handleFailure(error: ResponseMapperError.CannotParseResponse as NSError)
    }
  }
  
  private func handleFailure(error: NSError) {
    failure?(error)
    finish()
  }
}
