//
//  ServiceOperation.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/5/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

public class ServiceOperation: NetworkOperation {
  
  let service: BackendService
  
  public override init() {
    service = BackendService(conf: BackendConfiguration.shared)
    super.init()
  }
  
  public override func cancel() {
    service.cancel()
    super.cancel()
  }
  
}
