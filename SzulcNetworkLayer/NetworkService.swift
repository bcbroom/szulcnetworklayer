//
//  NetworkService.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/5/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

class NetworkService {

private var task: URLSessionDataTask?
  private var successCodes: Range<Int> = 200..<299
  private var failureCodes: Range<Int> = 400..<499
  
  enum Method: String {
    case GET, POST, PUT, DELETE
  }
  
  func request(url: URL,
               method: Method,
               params: [String: Any]? = nil,
               headers: [String: String]? = nil,
               success: ((Data?) -> Void)? = nil,
               failure: ((_ data: Data?, _ error: Error?, _ responseCode: Int) -> Void)? = nil) {
    
    var newRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)
    newRequest.allHTTPHeaderFields = headers
    newRequest.httpMethod = method.rawValue
    
    if let params = params {
      newRequest.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
    }
    
    let session = URLSession.shared
    task = session.dataTask(with: newRequest, completionHandler: { (data, response, error) in
      
      guard let httpResponse = response as? HTTPURLResponse else {
        failure?(data, error as? NSError, 0)
        return
      }
      
      if let error = error {
        failure?(data, error, httpResponse.statusCode)
        return
      }
      
      if self.successCodes.contains(httpResponse.statusCode) {
        success?(data)
      } else if self.failureCodes.contains(httpResponse.statusCode) {
        failure?(data, error as NSError?, httpResponse.statusCode)
      } else {
        let info = [ NSLocalizedDescriptionKey: "Request failed with code \(httpResponse.statusCode)",
          NSLocalizedFailureReasonErrorKey: "Server error." ]
        let error = NSError(domain: "NetworkService", code: 0, userInfo: info)
        failure?(data, error, httpResponse.statusCode)
      }
      
    })
    
    task?.resume()
    
  }
  
  func cancel() {
    task?.cancel()
  }
}
