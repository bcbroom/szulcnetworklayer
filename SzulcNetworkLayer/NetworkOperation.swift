//
//  NetworkOperation.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/5/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

public class NetworkOperation: Operation {


  private var _ready: Bool
  public override var isReady: Bool {
    get { return _ready }
    set { update(change: { self._ready = newValue }, key: "isReady") }
  }
  
  private var _executing: Bool
  public override var isExecuting: Bool {
    get { return _executing }
    set { update(change: { self._executing = newValue }, key: "isExecuting") }
  }
  
  private var _finished: Bool
  public override var isFinished: Bool {
    get { return _finished }
    set { update(change: { self._finished = newValue }, key: "isFinished") }
  }
  
  private var _cancelled: Bool
  public override var isCancelled: Bool {
    get { return _cancelled }
    set { update(change: { self._cancelled = newValue }, key: "isCancelled") }
  }
  
  private func update(change: (Void) -> Void, key: String) {
    willChangeValue(forKey: key)
    change()
    didChangeValue(forKey: key)
  }
  
  public override var isAsynchronous: Bool {
    return true
  }
  
  override init() {
    _ready = true
    _executing = false
    _finished = false
    _cancelled = false
    
    super.init()
    
    name = "Network Operation"
  }
  
  public override func start() {
    if self.isExecuting == false {
      isReady = false
      isExecuting = true
      isFinished = false
      isCancelled = false
    }
  }
  
  func finish() {
    isExecuting = false
    isFinished = true
  }
  
  public override func cancel() {
    isExecuting = false
    isCancelled = true
  }
  
}








