//
//  ViewController.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/5/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    
    
//    let todoOperation = TodoCreateOperation(userID: 1, title: "New Todo", completed: false)
//    todoOperation.success = { todo in
//      print(todo.title)
//    }
//    todoOperation.failure = { error in
//      print("Error: \(error.localizedDescription)")
//    }
//    todoOperation.start()
    
//    let getOne = TodoGetOperation(id: 2)
//    getOne.success = { todo in
//      print(todo.title)
//    }
//    getOne.failure = { error in
//      print("Error: \(error.localizedDescription)")
//    }
//    getOne.start()
    
    let getIndex = TodoGetIndexOperation()
    getIndex.success = { list in
      print(list.count)
      print(list[0].title)
    }
    getIndex.start()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

