//
//  TodoGetOperation.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/18/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

public class TodoGetOperation: ServiceOperation {
  private let request: TodoGetRequest
  
  public var success: ((TodoItem) -> Void)?
  public var failure: ((NSError) -> Void)?
  
  public init(id: Int) {
    request = TodoGetRequest(id: id)
    super.init()
  }
  
  public override func start() {
    super.start()
    service.request(request: request, success: handleSuccess, failure: handleFailure)
  }
  
  private func handleSuccess(response: Any?) {
    do {
      let todoItem = try TodoItemResponseMapper.process(obj: response)
      success?(todoItem)
      finish()
    } catch {
      handleFailure(error: ResponseMapperError.CannotParseResponse as NSError)
    }
  }
  
  private func handleFailure(error: NSError) {
    failure?(error)
    finish()
  }
}
