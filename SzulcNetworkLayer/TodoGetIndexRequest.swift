//
//  TodoGetIndexRequest.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/19/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

final class TodoGetIndexRequest: BackendAPIRequest {
  
  init() {
  }
  
  var endpoint: String {
    return "/todos/"
  }
  
  var method: NetworkService.Method {
    return .GET
  }
  
  var parameters: [String : Any]? {
    return nil
  }
  
  var headers: [String : String]? {
    return defaultJSONHeaders()
  }
  
}
