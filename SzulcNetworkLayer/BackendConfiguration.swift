//
//  BackendConfiguration.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/5/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

public final class BackendConfiguration {
  
  let baseURL: URL
  
  public init(baseURL: URL) {
    self.baseURL = baseURL
  }
  
  public static var shared: BackendConfiguration!
}
