//
//  BackendService.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/5/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

class BackendService {
  
  private let conf: BackendConfiguration
  private let service: NetworkService!
  
  init(conf: BackendConfiguration) {
    self.conf = conf
    self.service = NetworkService()
  }
  
  func request(request: BackendAPIRequest,
               success: ((Any?) -> Void)? = nil,
               failure: ((NSError) -> Void)? = nil) {
    
    let url = conf.baseURL.appendingPathComponent(request.endpoint)
    
    let headers = request.headers
    // set additional headers, such as auth
    //headers?["X-Api-Auth-Token"] = BackendAuth.shared.token
    
    service.request(url: url, method: request.method, params: request.parameters, headers: headers, success: { data in
      var json: Any? = nil
      
      if let data = data {
        json = try? JSONSerialization.jsonObject(with: data, options: [])
      }
      
      success?(json)
    }, failure: { data, error, statusCode in
      print(statusCode)
      // do whatever, then call failure block
      
    })
    
  }
  
  func cancel() {
    service.cancel()
  }
  
}
