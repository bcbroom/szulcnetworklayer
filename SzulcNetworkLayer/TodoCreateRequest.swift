//
//  TodoCreateRequest.swift
//  SzulcNetworkLayer
//
//  Created by Brian Broom on 1/5/17.
//  Copyright © 2017 Learning Objective. All rights reserved.
//

import Foundation

final class TodoCreateRequest: BackendAPIRequest {
  
  private let userID: Int
  private let title: String
  private let completed: Bool
  
  init(userID: Int, title: String, completed: Bool) {
    self.userID = userID
    self.title = title
    self.completed = completed
  }
  
  var endpoint: String {
    return "/todos"
  }
  
  var method: NetworkService.Method {
   return .POST
  }
  
  var parameters: [String : Any]? {
   return [
    "userId": userID,
    "title" : title,
    "completed": completed ]
  }
  
  var headers: [String : String]? {
    return defaultJSONHeaders()
  }
  
}
